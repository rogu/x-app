import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'cart', loadChildren: () => import('./pages/cart/cart.module').then((m) => m.CartModule) },
  { path: 'game', loadChildren: () => import('./pages/game/game.module').then((m) => m.GameModule) },
  { path: 'items', loadChildren: () => import('./pages/items/items.module').then((m) => m.ItemsModule) },
  { path: 'register', loadChildren: () => import('./pages/register/register.module').then((m) => m.RegisterModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
