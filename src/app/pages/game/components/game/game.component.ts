import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { GameService, Message } from "../../game.service";
import { catchError, switchMapTo, take, map, switchMap, retry, throttleTime } from 'rxjs/operators';
import { throwError, interval, of, fromEvent } from 'rxjs';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  @ViewChild('playerTpl') avatarTpl;

  constructor(
    private gameService: GameService,
    private container: ViewContainerRef
  ) {
    //interval(5000).pipe(take(1),switchMapTo(of(1))).subscribe(console.log)
  }

  ngOnInit(): void {
    this.gameService.getUser()
      .pipe(
        catchError((err) => {
          this.register();
          return throwError(err);
        })
      )
      .subscribe((resp) => {
        this.init()
      })
  }

  init() {
    this.gameService.messanger.subscribe((msg: Message) => {
      this.updateAvatar(msg);
    })

    fromEvent(document.body, 'mousemove')
      .pipe(
        throttleTime(30)
      )
      .subscribe(({ clientX, clientY }: MouseEvent) => {
        this.gameService.messanger.next({ clientX, clientY });
      })
  }
  updateAvatar(msg: Message) {
    this.container.createEmbeddedView(this.avatarTpl, { $implicit: msg });
  }

  register() {
    of('your name /^[a-zA-Z]{3,6}$/')
      .pipe(
        map(txt => prompt(txt)),
        switchMap(username => this.gameService.register(username)),
        catchError(({ error }) => {
          alert(JSON.stringify(error));
          return throwError(error);
        }),
        retry(1)
      )
      .subscribe(this.init.bind(this))
  }

}
