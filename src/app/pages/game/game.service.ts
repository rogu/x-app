import { Injectable } from '@angular/core';
import { webSocket } from "rxjs/webSocket";
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Api } from '../../utils/api';

export interface Message {
    username?: string;
    clientX?: number;
    clientY?: number;
    size?: number;
    action?: string;
    type?: string;
}

@Injectable({
    providedIn: 'root'
})
export class GameService {
    ws: Subject<any>;

    constructor(private http: HttpClient) { }

    register(username): Observable<any> {
        return this.http.post(Api.GAME_REGISTER_USER_END_POINT, { username }, { withCredentials: true });
    }

    getUser(): Observable<any> {
        return this.http.get(Api.GAME_GET_USER_END_POINT, { withCredentials: true });
    }

    get messanger(): Subject<Message> {
        return this.ws ? this.ws : this.ws = webSocket(Api.GAME_END_POINT);
    }
}
