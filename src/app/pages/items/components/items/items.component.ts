import { Component, OnInit } from '@angular/core';
import { ItemsApiService } from '../../services/items-api.service';
import { StoreService } from '../../../../services/store.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  items$: any;

  constructor(
    private itemsService: ItemsApiService,
    private store: StoreService
  ) { }

  ngOnInit(): void {
    this.items$ = this.itemsService.fetch()
  }

}
